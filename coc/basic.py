import random
import json
import asyncio
import common.response as res

def setupConditions():
    conditions = {}
    # おみくじ
    conditions['Fortune1'] = res.responseCondition('今日の運勢', needMention=False, channelName=['総合'])
    conditions['Fortune2'] = res.responseCondition('今日の運勢')
    # ダメです
    conditions['Dame'] = res.responseCondition('(OK|いい|良い|よい)[^A-Z]*？', regex=True, needMention=False, channelName=['総合'])

    return conditions 


async def response(reContent, message, Fortunetelled_User) -> bool:
    # ダメです
    if reContent == 'Dame':
        await message.channel.send('ダメです')
        return True
    # にゃーん
    elif reContent == 'Neko':
        await message.channel.send('にゃーん')
        return True
    # 今日の運勢
    elif ( reContent == 'Fortune1' ) or ( reContent == 'Fortune2' ) :
        if message.author.id in Fortunetelled_User:
            await message.channel.send('もう占いました。')
            return True
        else:
            with open('coc/fortune.json', 'r', encoding='utf-8') as f:
                fortune_list = json.load(f)
            fortune = random.choice(list(fortune_list.keys()))
            await message.channel.send(fortune)
            await message.channel.send('――' + fortune_list[fortune])
            Fortunetelled_User.append(message.author.id)
            return True
    else:
        return False

