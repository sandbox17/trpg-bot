import sys
import re
import requests
import json
import jaconv
from re import S

from requests.models import Response


def check_cs(args):
    cs_path = args[1]
    cs_data = get_cs_data(cs_path)
    # CSデータ取得失敗
    if cs_data == 0:
        return
    profession_data = {}

    with open('job_data.json', encoding='utf-8') as f:
        profession_data = json.load(f)

    # 職業データ検索
    if len(args) > 2:
        profession = get_profession(profession_data, args[2])
    else:
        profession = get_profession(
            profession_data, cs_data["parsonal_data"]["profession"])
    if profession:
        print(F'職業：{profession["ruled_name"]}に設定されています。')
    else:
        print('入力された職業に合致するデータが見つかりませんでした。')
        return

    # 職業PT計算チェック
    profession_points = calc_profession_point(
        profession["base_ablity"], cs_data["ability_value"])
    if max(profession_points) == cs_data['skill']['fix_profession_point']:
        print(F'【OK】職業PT：{max(profession_points)}')
    elif min(profession_points) == cs_data['skill']['fix_profession_point']:
        print(F'【要確認】職業PTが低い方の能力値で計算されています。これで良いですか？')
    else:
        print(F'【要修正】職業PTの計算が間違っています。')

    # 職業技能チェック
    result = evaluate_profession_skills(
        profession['skills'], profession['interpersonal_skill'], cs_data)
    if len(result) > profession['optional_skill']:
        print(
            F'【要修正】任意枠:{profession["optional_skill"]}に対し、多すぎる職業技能が設定されています。{result}')
    else:
        print('【OK】職業技能は規定通りです。')

    # 信用チェック
    credit_point = calc_credit_point(cs_data['negotiation_skill'])
    if credit_point < profession['min_credit']:
        print(F'【要修正】信用技能が職業に設定された値を下回っています。')
    elif credit_point > profession['max_credit']:
        print(F'【要確認】信用技能が職業に設定された値を上回っています。')
    else:
        print(F'【OK】信用技能の値は職業設定の範囲内です。')

    # キーコネチェック
    key_connection = count_key_connection(cs_data['backstory'])
    if key_connection > 1:
        print(F'【要修正】キーコネクションが複数設定されています。バックストーリーの内1つを選んでチェックして下さい。')
    elif key_connection == 0:
        print(F'【要修正】キーコネクションが設定されていません。バックストーリーの内1つを選んでチェックして下さい。')
    else:
        print(F'【OK】キーコネクション設定済みです。')

    # 年齢補正チェック
    age = get_age_degit(cs_data['parsonal_data']['age'])
    if age <= 0:
        print(F'【要確認】年齢が正しく記入されていません。年齢補正チェックに失敗しました。')
    elif is_age_fix(cs_data['ability_value'], age):
        print(F'【OK】年齢補正のチェックを完了しました。')
    else:
        print(F'【要修正】年齢補正が適用されていません。年齢に応じたステータス補正を適用して下さい。')


def get_cs_data(cs_path):
    # いあきゃら
    if cs_path.startswith('https://iachara.com'):
        regex_pattern = r'https://iachara.com/char/.*/(\d+)'
        try:
            cs_id = re.match(regex_pattern, cs_path).group(1)
        except AttributeError:
            print(F'想定していないURLです')
            return 0
        url = F'https://api.iachara.com/api/char/{cs_id}'
        response_body = requests.get(url).json()
        if response_body['success'] == False:
            print(F'キャラシが削除されたか、非公開の可能性があります')
            return 0
        return json.loads(response_body['result']['data'])
    else:
        with open(cs_path, encoding='utf-8') as f:
            return json.load(f)


def get_profession(profession_data, profession_entry):
    for profession in profession_data.values():
        if profession_entry in profession["names"]:
            return profession
    return {}


def calc_profession_point(base_ability, ability_value):
    # 職業PT算出に使用する能力値データを抜き出す
    base_ability_values = map(lambda x: ability_value[x], base_ability)
    # 増減値込みの能力値を算出するlambdaを定義
    def f(x): return int(x['value'] or "0") + int(x['fixed_diff'] or "0")
    # 職業ごとの能力値とEDUを足して２倍する
    return list(map(lambda x: (f(x) + f(ability_value['edu']))*2, base_ability_values))


def evaluate_profession_skills(profession_skills, interpersonal_num, cs_data):
    """
    return 職業設定に合致しなかった技能のset
    """
    # 職業PTが振られた技能を抽出
    skill_set = cs_data['battle_skill'] + cs_data['search_skill'] +\
        cs_data['action_skill'] + cs_data['negotiation_skill'] + \
        cs_data['knowledge_skill']
    result = filter(lambda x: int(x['profession_point'] or "0") > 0, skill_set)
    skill_list = list(map(lambda x: x['name'], result))

    try:
        skill_list.remove('信用')
    except ValueError:
        pass

    # 「または」で指定された職業技能は、合致するものを１つ除外する
    selection_list = filter(lambda x: type(x) != str, profession_skills)
    for selection in selection_list:
        for skill in selection:
            if skill in skill_list:
                skill_list.remove(skill)
                break

    # 職業技能に無い技能を抽出
    profession_skills = filter(lambda x: type(x) == str, profession_skills)
    skill_dif = set(skill_list) - set(profession_skills)
    interpersonal_skills = {'言いくるめ', '説得', '魅惑', '威圧'}
    for i in range(interpersonal_num):
        result = skill_dif & interpersonal_skills
        if result:
            skill_dif.remove(list(result)[0])

    return skill_dif


def calc_credit_point(negotiation_skill):
    credit = negotiation_skill[1]
    return int(credit['profession_point'] or "0") + int(credit['interest_point'] or "0")


def count_key_connection(backstory):
    count = 0
    for bk in backstory:
        if bk['checked']:
            count += 1
    return count


def get_age_degit(str):
    str = jaconv.z2h(str, kana=False, ascii=False, digit=True)
    str = re.sub(r"\D", "", str)
    try:
        age = int(str)
    except:
        return -1
    return age


def is_age_fix(ability_value, age):
    str_fix = int(ability_value['str']['fixed_diff'] or "0")
    siz_fix = int(ability_value['siz']['fixed_diff'] or "0")
    edu_fix = int(ability_value['edu']['fixed_diff'] or "0")
    app_fix = int(ability_value['app']['fixed_diff'] or "0")
    con_fix = int(ability_value['con']['fixed_diff'] or "0")
    dex_fix = int(ability_value['dex']['fixed_diff'] or "0")

    if age < 20:
        return ((edu_fix == -5) & ((str_fix + siz_fix) == -5))
    elif age < 40:
        return True
    elif age < 50:
        return ((app_fix == -5) & ((str_fix + con_fix + dex_fix) == -5))
    elif age < 60:
        return ((app_fix == -10) & ((str_fix + con_fix + dex_fix) == -10))
    elif age < 70:
        return ((app_fix == -15) & ((str_fix + con_fix + dex_fix) == -20))
    elif age < 80:
        return ((app_fix == -20) & ((str_fix + con_fix + dex_fix) == -40))
    elif age < 90:
        return ((app_fix == -25) & ((str_fix + con_fix + dex_fix) == -80))


if __name__ == '__main__':
    args = sys.argv
    check_cs(args)
