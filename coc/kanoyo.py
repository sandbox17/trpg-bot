from enum import IntEnum
from collections import OrderedDict
import discord
import yaml
import sys
import random
import re  # 正規表現
from common.response import responseCondition as kanoyoRC
import asyncio


class Level(IntEnum):
    High = 3
    Middle = 2
    Low = 1


class General:
    def __init__(self, client, config):
        self.client = client

        # インスタンスを起動
        print('カノヨ街インスタンスを始動。\nKPはダイスBotの起動を確認してください。')
        self.call = Calling(client, config['Kanoyo-Channels'])
        self.hack = Hacking(config['Kanoyo-Config'])

        self.opening = False
        self.alerted = False

        # オートロード
        if self.loadData(self.hack, 'kanoyo_auto_savedata.txt'):
            print('オートセーブデータ`kanoyo_auto_savedata.txt`を読み込みました。')
            print('新規ゲームを開始する場合、セーブデータを削除して再起動してください。')

    def setupConditions(self):
        conditions = OrderedDict()
        hoChannel = ['ho1', 'ho2', 'ho3', 'ho4']
        conditions['set_hack'] = kanoyoRC(
            r'set:\d+', regex=True, onlyForOwner=True)
        conditions['start'] = kanoyoRC('カノヨ街・開闢', onlyForOwner=True)
        conditions['end_kp'] = kanoyoRC('カノヨ街・停止', onlyForOwner=True)
        conditions['save'] = kanoyoRC('セーブ', onlyForOwner=True)
        conditions['load'] = kanoyoRC('ロード', onlyForOwner=True)
        conditions['test'] = kanoyoRC('テスト', onlyForOwner=True)
        conditions['open'] = kanoyoRC(r'\d\d開示', regex=True, onlyForOwner=True)
        conditions['end_pc'] = kanoyoRC('3842877')
        conditions['reduce'] = kanoyoRC(
            'ハッキング成功', onlyForOwner=True, channelName=hoChannel)
        conditions['hack'] = kanoyoRC('ハッキング', channelName=hoChannel)
        conditions['tansu'] = kanoyoRC('薬箪笥', channelName=hoChannel)
        conditions['kano'] = kanoyoRC(r'カノ|管理人', regex=True, channelName=hoChannel)
        conditions['npc'] = kanoyoRC(
            r'PC1|PC2|PC3|PC4|警邏提灯|唐傘薬師|木綿神官', regex=True, channelName=hoChannel)
        conditions['other'] = kanoyoRC(
            r'.*', regex=True, channelName=hoChannel)
        return conditions

    async def response(self, reContent, message):
        # オープニングを4チャンネル同時に実行する
        if reContent == 'start':
            self.opening = True
            await asyncio.gather(
                self.call.opening('ho1'),
                self.call.opening('ho2'),
                self.call.opening('ho3'),
                self.call.opening('ho4')
            )
            self.opening = False
            return
        elif reContent == 'set_hack':
            await message.channel.send(self.hack.setHackingNo(message.content))
            return

        elif reContent == 'save':
            await message.channel.send('プレイデータをセーブします。')
            if self.saveData(self.hack, filename = 'kanoyo_savedata.txt'):
                await message.channel.send('完了しました。ファイル名：`kanoyo_savedata.txt`')
            else:
                await message.channel.send('セーブに失敗しました。')
            return

        elif reContent == 'load':
            await message.channel.send('プレイデータ`kanoyo_savedata.txt`をロードします。')
            if self.loadData(self.hack, 'kanoyo_savedata.txt'):                
                await message.channel.send('完了しました。')
            else:
                await message.channel.send('ロードに失敗しました。')
            await self.call.setDiceBot(message.channel)
            return
        
        elif reContent == 'test':
            await self.call.sendMessage2AllHO('テストです。')
            return

        elif reContent == 'end_kp' or reContent == 'end_pc':
            await self.call.stopKanoyo()
            await self.client.close()
            return

        # オープニングの間はハッキング抑止
        if not self.opening:
            if reContent == 'hack' or reContent == 'npc':
                hack_no = await self.hack.checkHacking(message, reContent)
                isHackNoLast = await self.hack.checkHackNo(message)
                if hack_no and isHackNoLast:
                    await self.hack.executeHacking(self.client, message, hack_no)
                self.saveData(self.hack, 'kanoyo_auto_savedata.txt')
            elif reContent == 'tansu':
                text = '薬箪笥はハッキング可能です。'
                hack_no = '90'
                await message.channel.send(text)
                if await self.hack.checkHackNo(message):
                    await self.hack.executeHacking(self.client, message, hack_no)
                    text = message.channel.guild.owner.mention + \
                        ' 薬箪笥へのハッキングが実行されました。'
                    await message.channel.send(text)
                self.saveData(self.hack, 'kanoyo_auto_savedata.txt')
            elif reContent == 'kano':
                text = 'カノはハッキング可能です。実行する場合はメインタブで宣言お願いします。'
                await message.channel.send(text)
            elif reContent == 'other':
                text = '対象オブジェクトはハッキングできません。'
                await message.channel.send(text)
            elif reContent == 'reduce':
                text = self.hack.reduceHackNo(message.channel.name)
                await message.channel.send(text)
                self.saveData(self.hack, 'kanoyo_auto_savedata.txt')
            elif reContent == 'open':
                match = re.findall(r'(\d\d)開示', message.content)
                hack_no = match[0]
                if hack_no in self.hack.active_hack_list:
                    self.hack.active_hack_list.remove(hack_no)
                text = F'`{self.hack.hack_list[hack_no]}`の情報を開示します。'
                await message.channel.send(text)
                await self.hack.sendHackingInfo(message.channel, hack_no)
                self.saveData(self.hack, 'kanoyo_auto_savedata.txt')

            # 閾値チェック
            if self.hack.checkThreshold() and self.alerted == False:
                self.alerted = True
                text = '**ハッキング回数が許容値を超えました。カノヨ街が警戒態勢へ移行しました。**'
                await self.call.sendMessage2AllHO(text)

        return

    def saveData(self, hack, filename):
        """ ハッキング状態をファイルに書き出す。

        Arguments:
            hack {Hacking} -- Hackingインスタンス。メンバ変数を取り出す。

        Returns:
            [boolean] -- 書き込み成否
        """
        try:
            with open(filename, mode='w', encoding='utf-8') as f:
                savedata = {}
                savedata['hack_count'] = hack.hack_count
                savedata['hack_list'] = hack.active_hack_list
                f.writelines(yaml.dump(savedata, default_style='\''))
        except:
            return False
        else:
            return True

    def loadData(self, hack, filename):
        """ 中断データを読み込み、状態を元に戻す。

        Arguments:
            hack {Hacking} -- Hackingインスタンス。メンバ変数にアクセスする。

        Returns:
            [boolean] -- 読み込み成否
        """
        try:
            with open(filename, 'r', encoding='utf-8') as f:
                loaddata = yaml.safe_load(f)
                hack.hack_count = loaddata['hack_count']
                hack.active_hack_list = loaddata['hack_list']
        except:
            return False
        else:
            # 警報発令済みかチェック
            if self.hack.checkThreshold():
                self.alerted = True
            return True


class Calling:
    def __init__(self, client, channel_setting):
        self.client = client
        # チャンネル設定
        self.channel = {}
        channel_keys = ['ho1', 'ho2', 'ho3', 'ho4', 'all']
        for channel_key in channel_keys:
            try:
                channel_obj = client.get_channel(
                    channel_setting.getint(channel_key))
            except ValueError:
                print('Botの起動中に問題が発生しました。')
                print(F'{channel_key}のチャンネルID指定が不正です。settings.iniを確認してください。')
                sys.exit()
            else:
                if channel_obj:
                    self.channel[channel_key] = channel_obj
                else:
                    print('Botの起動中に問題が発生しました。')
                    print(F'{channel_key}のチャンネルID指定が不正です。settings.iniを確認してください。')
                    sys.exit()

        # ダイス起動コマンド
        if channel_setting.getint('edition') == 7:
            self.setDiceCommand = 'bcdice set Cthulhu7th'
        else:
            self.setDiceCommand = 'bcdice set Cthulhu'

    # メッセージ一斉送信
    async def sendMessage2AllHO(self, text):
        await asyncio.gather(
            self.channel['ho1'].send(text),
            self.channel['ho2'].send(text),
            self.channel['ho3'].send(text),
            self.channel['ho4'].send(text)
        )
        return

    # シナリオ開始時
    async def opening(self, ho_no):
        channel = self.channel[ho_no]
        await channel.send('『カノヨ街』にログインします。')
        await channel.send('・・・認証中・・・')
        await channel.send('・・・ログイン情報を確認・・・')
        await channel.send('・・・このアカウントには特殊コマンド`ハッキング`が登録されています。\n' +
                           '判定用ダイス機能を起動します。')
        await channel.send(self.setDiceCommand)
        await channel.send('アカウントの連携を確認します。\n' +
                           'このBotにメンションする形で`ハッキング`と発言してください。')

        # PLの応答を待機
        def check1(m):
            return 'ハッキング' in m.content \
                and m.channel == channel \
                and self.client.user in m.mentions
        await self.client.wait_for('message', check=check1)
        await channel.send('確認できました。')
        await channel.send('続いて、判定機能を動作確認します。\n' +
                           '`CC<=50`を実行してください。メンションは不要です。')

        # ダイスの反応を待機
        def check2(m):
            pattern = r'(DiceBot|Cthulhu|Cthulhu7th|\(1D100).*'
            return re.search(pattern, m.content) \
                and m.channel == channel
        await self.client.wait_for('message', check=check2)
        await channel.send('問題ありません。')
        await channel.send('なお、NPC/PCにコマンドを実行する場合、対象の名称をこのBotにメンション付きで発言して下さい。\n' +
                           'PC名は、「PC1」「PC2」「PC3」「PC4」で識別されます。\n' +
                           F'例）`@{self.client.user.name} PC1`')
        await channel.send('・・・')
        await channel.send('『カノヨ街』へ侵入成功。スキャン回避のため、待機モードへ移行します。\n' +
                           '以降、不明点はKPに問い合わせください。')

    # ダイスの再設定
    async def setDiceBot(self, channel):
        await channel.send('ダイスBotを再設定しますか(y/n)？\n' +
                           '※ダイスBotを再起動した場合、"y"を入力してください')

        # YN入力を待機
        def check(m):
            return re.search('y|n', m.content) and m.channel == channel
        response = await self.client.wait_for('message', check=check)
        if 'y' in response.content:
            await self.sendMessage2AllHO(self.setDiceCommand)
        return

    # 前編終了時
    async def stopKanoyo(self):
        channel = self.channel['all']
        await channel.send('入力コード:3842877')
        await asyncio.sleep(2)
        await channel.send('停止コードの入力を確認しました。\n' +
                           '只今より、「カノヨ街」停止作業を開始します...')
        await asyncio.sleep(1)
        await channel.send('Running:KANOYOGAI を停止します...50％...80％...')
        await asyncio.sleep(2)
        await channel.send('「KANOYOGAI」停止コード実行により、全ての患者ライセンスを剥奪し、電子パッケージ化作業に入ります。')
        await asyncio.sleep(1)
        await channel.send('「AI no Gyoka」起動まで、推定残り時間 70 秒...')
        await asyncio.sleep(2)
        await channel.send('Scanning:電子パッケージ化作業/FALSE...4 件\n' +
                           '作業を続行しますか？ y/n')
        await channel.send('y')
        await asyncio.sleep(1)
        await channel.send('Decision:KANOYOGAI を停止します...')
        await asyncio.sleep(3)
        await channel.send('COMPLETE/TRUE')
        await asyncio.sleep(1)
        await channel.send('"Good Night"')
        return


class Hacking:
    def __init__(self, config):
        # ハッキング対象を設定
        contents = ''
        with open('coc/kanoyo_data/hack_object_list.txt', 'r', encoding='utf-8') as f:
            contents = f.read()
            print()
            print('■ハッキング番号表■')
            print(contents)
            print()
        self.hack_list = yaml.safe_load(contents)

        # ハッキング状況
        self.active_hack_no = '0'
        self.hack_count = {
            'ho1': 3,
            'ho2': 3,
            'ho3': 3,
            'ho4': 3
        }
        self.active_hack_list = list(self.hack_list.keys())

        # 警戒レベルの設定
        self.alert_level = config.getint('alert_level')
        self.alert_threshold = config.getint('alert_threshold')

        # クリファン対応の設定
        self.cf_operation = config.getint('cf_operation')
        self.cf_option = config.getint('cf_option')

    async def checkHacking(self, message, target):
        """ ハッキングを要求された際、Botが処理可能かを判定する。
        Arguments:
            message {Message} -- ハッキング要求メッセージ

        Returns:
            [str] -- ハッキング対象No.（対処不可の場合は空文字）
        """
        # 対象がハッキング可能な場合
        if target == 'npc' or self.active_hack_no != '0':
            hack_target = ''
            hack_no = ''
            if target == 'npc':
                # メッセージからオブジェクト名を取り出す
                pattern = r'PC1|PC2|PC3|PC4|警邏提灯|唐傘薬師|木綿神官'
                match = re.search(pattern, message.content)
                hack_target = match.group(0)
                hack_no = '99'
            else:
                # 現在有効なオブジェクト名を取り出す
                hack_target = self.hack_list[self.active_hack_no]
                hack_no = self.active_hack_no
            await message.channel.send(F'`{hack_target}`はハッキング可能です。')
            return hack_no
        else:
            await message.channel.send('今ハッキングできるオブジェクトはありません。')
            return ''

    async def checkHackNo(self, message):
        """ ハッキング残数が残っているか確認する
        Arguments:
            message {Message} -- ハッキング要求メッセージ

        Returns:
            [boolean] -- 残っていない場合False
        """
        ho_no = message.channel.name
        if self.hack_count[ho_no] <= 0:
            text = 'あなたは既にハッキングを3回以上実行しています。'\
                'ハッキングを強行する場合、KPに要請してください。'
            await message.channel.send(text)
            await message.channel.send(message.channel.guild.owner.mention)
            return False
        return True

    async def executeHacking(self, client, message, hack_no):
        """ハッキングを実行する

        Arguments:
            client {[type]} -- [description]
            message {[type]} -- [description]

        Returns:
            [bool] -- ハッキングの成否
        """
        text = 'ハッキングを実行する場合、`コンピューター`ロールを行ってください（１分以内）。'
        await message.channel.send(text)

        # ダイスBotの判定を待機
        self.isBotMentioned = False

        def check(m):
            pattern = r'(DiceBot|Cthulhu|Cthulhu7th|\(1D100).*'
            isDiceRoled = re.search(
                pattern, m.content) and m.channel == message.channel
            # KP Botにメンションされたら処理を終了させる
            self.isBotMentioned = client.user in m.mentions and m.channel == message.channel
            return isDiceRoled or self.isBotMentioned

        try:
            msg = await client.wait_for('message', timeout=60.0, check=check)
        except asyncio.TimeoutError:
            await message.channel.send('制限時間をオーバーしました。')
            return False
        else:
            if self.isBotMentioned:
                return False
            return await self.returnHackResult(msg, hack_no)

    async def returnHackResult(self, message, hack_no):
        """ ハッキングのダイスに応じて結果を返す

        Arguments:
            message {Message} -- ダイスの応答メッセージ
            hack_no {str} -- ハッキング対象No. NPCは'90'か'99'
        Returns:
            [bool] -- ハッキングの成否
        """
        ho_no = message.channel.name
        # ハッキング対象が現在有効か確認する
        if (hack_no != self.active_hack_no) and (hack_no != '90') and (hack_no != '99'):
            await message.channel.send(F'対象から離れたため、ハッキングに失敗しました。（ハッキング残数：{self.hack_count[ho_no]}）')
            await message.channel.send(message.channel.guild.owner.mention)
            return False
        # ダイス結果確認
        if ('決定的成功' in message.content) or ('クリティカル' in message.content):
            if self.cf_operation != 0:
                return await self.responseCritical(ho_no, message.channel, hack_no)

        if re.search(r'成功|スペシャル', message.content):
            await message.channel.send('ハッキングを実行します・・・')
            # NPCの場合
            if hack_no == '99':
                await self.sendHackingInfo(message.channel, hack_no)
                await self.judgeAlertLevel(ho_no, message.channel)
                return False
            
            # 薬箪笥の場合
            if hack_no == '90':
                self.hack_count[ho_no] -= 1
                print(F'薬箪笥へのハッキングに{ho_no}が成功しました')
                await message.channel.send(F'ハッキングに成功しました。（ハッキング残数：{self.hack_count[ho_no]}）')
                await self.sendHackingInfo(message.channel, hack_no)
                return True

            # ハッキング済みオブジェクトの場合
            elif hack_no not in self.active_hack_list:
                hack_no += '-2'
                await self.sendHackingInfo(message.channel, hack_no)
                await self.judgeAlertLevel(ho_no, message.channel)
                return False
            # 通常オブジェクトの場合
            else:
                self.active_hack_list.remove(hack_no)
                self.hack_count[ho_no] -= 1
                print(F'情報No.{hack_no}を{ho_no}が回収しました')
                await message.channel.send(F'ハッキングに成功しました。（ハッキング残数：{self.hack_count[ho_no]}）')
                await self.sendHackingInfo(message.channel, hack_no)
                return True
        elif ('致命的失敗' in message.content) or ('ファンブル' in message.content):
            if self.cf_operation != 0:
                await self.responseFamble(ho_no, message.channel, hack_no)
                return False
        else:
            await message.channel.send(F'ハッキングに失敗しました。（ハッキング残数：{self.hack_count[ho_no]}）')
        return False

    async def responseCritical(self, ho_no, channel, hack_no):
        """クリティカルに対応する
        共通：対象がNPCの場合、ハッキング残数減なし（警戒レベル1と同じ）
        オプションが1の場合：ハッキング残数を減らさず情報を取得する。ハッキング済みオブジェクトでも強制的に情報を取得する。
        オプションが2の場合：次回の判定に補正（メッセージのみ）
        
        Arguments:
            ho_no {str} -- 応答中のHO番号
            channel {Channel} -- 応答中のチャンネル
            hack_no {str} -- ハッキング対象No.
        Returns:
            [bool] -- ハッキングの成否（NPCの場合Falseになる）
        """

        text = '決定的成功。独自仮想ネットワークを構築しました。'
        await channel.send(text)
        # NPCの場合
        if (hack_no == '90') or (hack_no == '99'):
            await self.sendHackingInfo(channel, hack_no)
            text = F'仮想ネットワークインスタンスを消去。アクセスログ検知を回避しました。（ハッキング残数：{self.hack_count[ho_no]}）'
            await channel.send(text)
            return False

        # 通常オブジェクトの場合
        if self.cf_operation == 1:
            if hack_no in self.active_hack_list:
                self.active_hack_list.remove(hack_no)
            print(F'情報No.{hack_no}を{ho_no}が回収しました')
            text = F'カノヨ街監視システムの回避に成功・・・\nハッキング完了しました。（ハッキング残数：{self.hack_count[ho_no]}）'
            await channel.send(text)
            await self.sendHackingInfo(channel, hack_no)
            return True
            
        elif self.cf_operation == 2:
            result = True
            await channel.send('接続中・・・')
            # ハッキング済みオブジェクトの場合
            if hack_no not in self.active_hack_list:
                hack_no += '-2'
                await self.sendHackingInfo(channel, hack_no)
                await self.judgeAlertLevel(ho_no, channel)
                result = False
            else:
                print(F'情報No.{hack_no}を{ho_no}が回収しました')
                self.hack_count[ho_no] -= 1
                self.active_hack_list.remove(hack_no)
                text = F'ハッキングに成功しました。（ハッキング残数：{self.hack_count[ho_no]}）'
                await channel.send(text)
                await self.sendHackingInfo(channel, hack_no)
            text = F'仮想ネットワークはあと１度接続可能です。\n次回ハッキング時に+{self.cf_option}%の補正を加えて判定して下さい。'
            await channel.send(text)
            return result

    async def responseFamble(self, ho_no, channel, hack_no):
        """ファンブルに対応する
        オプションが1の場合：ハッキング残数を１減らす
        オプションが2の場合：次回の判定に補正（メッセージのみ）

        Arguments:
            ho_no {str} -- 応答中のHO番号
            channel {Channel} -- 応答中のチャンネル
            hack_no {str} -- ハッキング対象No.
        """
        text = '致命的失敗。カノヨ街のスキャニング用プロファイルが更新されました。'
        await channel.send(text)
        if self.cf_operation == 1:
            self.hack_count[ho_no] -= 1
            text = F'コマンド実行を抑止し、スキャンを回避します。（ハッキング残数：{self.hack_count[ho_no]}）'
        elif self.cf_operation == 2:
            text = F'コマンド実行を抑止し、スキャンを回避します。\n次回ハッキング時に-{self.cf_option}%の補正を加えて判定して下さい。'
        await channel.send(text)
        return 

    async def judgeAlertLevel(self, ho_no, channel):
        """ハッキング済みのオブジェクトか、薬箪笥以外のNPCへの
        ハッキング成功した際、ハッキング残数を減らすか判定し処理する
        Arguments:
            ho_no {str} -- 対象のHO番号
            channel {Channel} -- 返信先チャンネル
        """
        judge = False
        if self.alert_level == Level.High:
            judge = True
        elif self.alert_level == Level.Middle:
            judge = random.choice((True, False))

        if judge:
            self.hack_count[ho_no] -= 1
            await channel.send(F'アクセス履歴の消去に失敗。\nカノヨ街の警戒レベルが上がりました。（ハッキング残数：{self.hack_count[ho_no]}）')
        else:
            await channel.send(F'アクセス履歴を消去し、接続を切断しました。（ハッキング残数：{self.hack_count[ho_no]}）')

    async def sendHackingInfo(self, channel, hack_no):
        """ ハッキング成功したNo.に対応する画像を送信する

        Arguments:
            channel {Channel} -- 送信対象チャンネル
            hack_no {str} -- ハッキング成功した情報No.
        """
        filename = hack_no + '.png'
        file = discord.File('coc/kanoyo_data/' + filename, filename=filename)
        await channel.send(file=file)

    def checkThreshold(self):
        """ハッキング残数が閾値を下回ったかチェックする

        Returns:
            [boolean] -- 下回った場合にはTrue
        """
        return self.alert_threshold > sum(self.hack_count.values())

    def reduceHackNo(self, ho_no: str):
        """ KP用。手動でハッキングを行った場合に、残ハッキング数を強制的に１減らす。

        Arguments:
            ho_no {str} -- 対象HOのNo.

        Returns:
            [str] -- ハッキング残数を示す応答メッセージ
        """
        self.hack_count[ho_no] -= 1
        text = F'{ho_no}のハッキング残数を減算しました。（ハッキング残数：{self.hack_count[ho_no]}）'
        return text

    def setHackingNo(self, messageTxt: str):
        """ KP用。現在ハッキング可能な対象Noを指定する

        Arguments:
            messageTxt {str} -- 送信されたテキスト

        Returns:
            [str] -- 応答テキスト
        """
        try:
            match = re.findall(r'set:(\d+)', messageTxt)
            hack_no = match[0]
            if hack_no == '0':
                self.active_hack_no = '0'
                return 'ハッキング対象をリセットしました。'
            hack_target = self.hack_list[hack_no]
        except:
            return '指定が正しくありません'
        else:
            self.active_hack_no = hack_no
            return F'ハッキングNo.{hack_no}：`{hack_target}`に設定しました。'
