import random
import common.response as res


def setupConditions():
    conditions = {}
    conditions['GM'] = res.responseCondition('GMの強権', onlyForOwner=True)
    conditions['PL'] = res.responseCondition('PLの越権', onlyForOwner=True)

    return conditions


def createMessage_GM():
    gm_list = [
        '【アザトースのうわ言・天啓】',
        '【アザトース寝返り・転移】',
        '【アザトースの気まぐれ・強化】'
    ]
    return random.choice(gm_list)


def createMessage_PL():
    pl_list = {
        'CON': '虚弱',
        'STR': '虚脱',
        'SIZ': '縮退',
        'DEX': '愚鈍',
        'POW': '薄弱',
        'EDU': '無学',
        'INT': '無知',
        'APP': '醜悪'
    }
    action = random.choice(list(pl_list.keys()))
    return F'【{pl_list[action]}の月】\n　――{action}１点を代償にSAN10点回復または狂気を１つ取り消す'
