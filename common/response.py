import re  # 正規表現


class responseCondition():
    """レスポンス条件設定クラス

    Returns:
        responseCondition -- 投稿に対しレスポンスを行う条件セット
    """

    def __init__(self, keyWord, regex=False, needMention=True, onlyForOwner=False, channelName=None):
        self.keyword = keyWord
        self.regex = regex
        self.needMention = needMention
        self.onlyForOwner = onlyForOwner
        self.channelName = channelName

    def checkMessage(self, messageTxt):
        # レスポンス対象メッセージか確認
        if self.regex:
            if re.search(self.keyword, messageTxt):
                return True
        else:
            if self.keyword in messageTxt:
                return True
        return False

    def checkCondition(self, message, bot):
        # レスポンス条件に適合するか確認
        if self.needMention:
            if bot not in message.mentions:
                return False
        if self.onlyForOwner:
            if message.author != message.guild.owner:
                return False
        if self.channelName:
            if message.channel.name not in self.channelName:
                return False
        return True
