# 導入方法

セットアップ手順の説明です。

前提として、基礎的なWindowsの操作を習得されていることを想定しています。

- zip解凍
- ファイルパス取得
- 拡張子の表示

この辺りが「？？」という方は厳しいと思います。

また、Macはサポート外となります。

## Botアカウントの作成・招待

[Discord公式ページ](https://discordpy.readthedocs.io/ja/latest/discord.html)を参考に、Botのアカウントを作成します。

「Botアカウント作成」の「6.他人にBotの招待を許可する場合～」まで完了したら、
**Privileged Gateway Intentsの下にある、SERVER MEMBERS INTENTをONにします**

続いて、上記ページの「Botを招待する」手順に沿って、必要なサーバにBotアカウントを招待してください。

「6.Bot Permissions～」では、**「Send Messages」と「Attach Files」を有効にしてください。**


## Botインストール

下記のリンクからダウンロードしてください。

* [KPBot_ver1.5.zip](https://gitlab.com/sandbox17/trpg-bot/-/raw/master/release/KPBot_ver1.5.zip)
* [ソースコードver1.4（開発者向け）](https://gitlab.com/sandbox17/trpg-bot/-/archive/ver1.5/trpg-bot-ver1.5.zip)
* [StellarBot_ver1.0.zip](https://gitlab.com/sandbox17/trpg-bot/-/raw/master/release/StellarBot_ver1.0.zip)

ダウンロード後、ファイルを解凍して展開してください。

その際、**ファイルパスに日本語を含まない場所に置いてください。**

OK）C:\TRPG\kp-bot

NG）C:\Users\山田太郎\Documents\kp-bot

## 初期設定

#### トークン設定

フォルダ内の*settings.ini*を編集し、*%(DISCORD_KPBOT_TOKEN)s*（または*%(DISCORD_STELLARBOT_TOKEN)s*）という文字列を、Botアカウント作成時に取得したトークンに置き換えてください。

環境変数の設定、と聞いて分かる方は、*DISCORD_KPBOT_TOKEN*・*DISCORD_STELLARBOT_TOKEN*というユーザ変数を設定してもらっても構いません（作者はそうしています）。


## 起動

それぞれ.exeファイルのダブルクリックで起動します。
