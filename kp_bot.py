import discord
import configparser
import os
import sys
from collections import OrderedDict
import common.response as res
from coc import basic, kanoyo, sokou


class discordBot(discord.Client):
    def __init__(self, scenario, config):
        #Memberオブジェクトへのアクセス権限を渡す（owner情報が取れないため）
        intents = discord.Intents.default()
        intents.members = True
        super().__init__(intents = intents)
        self.config = config

        # おみくじ機能用。占い済みユーザ名
        self.Fortunetelled_User = []

    async def on_ready(self):
        # シナリオ専用インスタンス生成
        if scenario == 'kanoyo':
            self.kanoyo = kanoyo.General(self, self.config)

        # 応答設定
        self.reConditions = self.setupConditions(scenario)
        print('Discordにログインしました。')

    async def on_message(self, message):
        # レスポンスコンテンツを検索
        reContent = ''
        for conditionName, reCondition in self.reConditions.items():
            if not reCondition.checkMessage(message.content):
                continue
            if reCondition.checkCondition(message, self.user):
                reContent = conditionName
                break

        # 基本応答
        if (await basic.response(reContent, message, self.Fortunetelled_User)):
            return

        # 仰ぎ見る遡行
        elif reContent == 'GM':
            await message.channel.send(sokou.createMessage_GM())
            return
        elif reContent == 'PL':
            await message.channel.send(sokou.createMessage_PL())
            return

        # カノヨ街
        if scenario == 'kanoyo':
            await self.kanoyo.response(reContent, message)

    def setupConditions(self, scenario):
        """応答条件を設定する。

        Arguments:
            scenario {str} -- 起動引数で指定するシナリオ名

        Returns:
            {orderedDict} -- 応答条件のリスト
        """
        conditions = OrderedDict()
        conditions.update(basic.setupConditions())

        if scenario == 'sokou':
            conditions.update(sokou.setupConditions())

        if scenario == 'kanoyo':
            conditions.update(self.kanoyo.setupConditions())
        # neko
        conditions['Neko'] = res.responseCondition('neko', needMention=False)

        return conditions


if __name__ == '__main__':
    # bot内容のセットアップ
    args = sys.argv
    scenario = None
    if len(args) > 1:
        scenario = args[1]

    # botの接続と起動
    config = configparser.ConfigParser(os.environ)
    config.read('settings.ini', encoding='utf-8')
    bot = discordBot(scenario, config)
    bot.run(config['General']['kpbot_token'])
