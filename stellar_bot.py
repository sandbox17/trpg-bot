import discord
import random
import configparser
import os


client = discord.Client()  # 接続に使用するオブジェクト
global bouquet

f = open('situation.txt', mode='r', encoding='shift_jis')
situation_list = f.readlines()
f.close()

# 起動時に通知してくれる処理
@client.event
async def on_ready():
    print('ログインしました')

# ブーケカウント処理
@client.event
async def on_message(message):
    global bouquet
    if client.user in message.mentions:
        if 'アクション' in message.content:
            bouquet = 0
            await message.channel.send('新規シーンを開始します')
            return
        elif 'カット' in message.content:
            try:
                await message.channel.send('ブーケの数は' + str(bouquet) + 'です')
            except:
                await message.channel.send('シーンを開始してください')
            return
        elif 'シチュエーション' in message.content:
                situation = random.choice(situation_list)
                await message.channel.send(f'お題：{situation}')
                return
    if '💐' in message.content:
        num = message.content.count('💐')
        if num > 5 :
            num = 5
        bouquet += num

# botの接続と起動
config = configparser.ConfigParser(os.environ)
config.read('settings.ini', encoding='utf-8')
client.run(config['General']['stellarbot_token'])
